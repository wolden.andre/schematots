export enum ElementType {
    FILE = "FILE",
    FOLDER = "FOLDER"
}

export interface TreeElement {
    name: string;
    path: string;
    type: ElementType;
    foldercontent?: TreeElement[];
}