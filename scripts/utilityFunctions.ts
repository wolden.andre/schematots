import * as fs from "fs";
import {resolve} from "path";
import {ElementType, TreeElement} from "./types";
import {compileFromFile} from "json-schema-to-typescript";

export const printNewLine = () => {
    console.warn("\n");
};

export const isDirectory: (path: string) => boolean =
    elementpath => fs.existsSync(elementpath) && fs.lstatSync(elementpath).isDirectory();

const createDirIfnotExists = (dir: string) => !fs.existsSync(dir) && fs.mkdirSync(dir, {recursive: true});

export const getTreeFor = (path: string): TreeElement[] => {
    const aDir: string[] = fs.readdirSync(path);
    const elementList: TreeElement[] = aDir.map((name: string) => {
        const elementpath = resolve(path, name);
        return isDirectory(elementpath) ? {
            name: name,
            path: elementpath,
            type: ElementType.FOLDER,
            foldercontent: getTreeFor(elementpath)
        } : {
            name: name,
            path: elementpath,
            type: ElementType.FILE
        };
    });
    return elementList;
};

export const mapKeysAndLocateRefs = (jsonSchema: string): string[] => {


    const go = (jsonSchema: string): string[] => {

        const strings: string[][] = Object.keys(jsonSchema).map((a: string) => {
            const jsonSchemaElement: any = jsonSchema[a as any];

            if (a === '$ref' && jsonSchemaElement && typeof jsonSchemaElement === 'string') {
                return [jsonSchemaElement as string];
            } else if(jsonSchemaElement && typeof jsonSchemaElement === "object") {
                return go(jsonSchemaElement);
            } else {
                return [];
            }
        });

        return strings.flatMap((value: string[]) => value)
    };
    return go(jsonSchema);
};

const addImportStatements = (treeElement: TreeElement, ts: string) => {
    const buffer = fs.readFileSync(treeElement.path, "utf8");
    const jsonSchema = JSON.parse(buffer);
    const strings: (string | string[])[] = mapKeysAndLocateRefs(jsonSchema);

    strings.forEach((typeFilePath: string) => {
        const jsonFilePath = resolve(treeElement.path, "../", typeFilePath);
        const refBuffer = fs.readFileSync(jsonFilePath, 'utf8');
        const refJson = JSON.parse(refBuffer);
        const NameOfType = refJson['title'];

        if (refJson && NameOfType) {
            ts = `import { ${NameOfType} } from '${typeFilePath}'; \n\n\n` + ts;
        }
    });
    return ts;
};

export const generateTsDeclarations = (tree: TreeElement[], outputdirectory: string) => {

    createDirIfnotExists(outputdirectory);
    console.warn(`Created output directory: ${outputdirectory}`);

    tree.map((treeElement: TreeElement) => {
        if (treeElement.type === ElementType.FILE) {

            compileFromFile(treeElement.path, { bannerComment: null, declareExternallyReferenced: false})
                .then(ts => {
                    ts = addImportStatements(treeElement, ts);
                    const outputPath = resolve(outputdirectory, treeElement.name + ".d.ts");
                    fs.writeFileSync(outputPath, ts)
                    console.warn(`Created file: ${outputPath}`);
                });
        } else {

            const subDirectory = resolve(outputdirectory, treeElement.name);

            createDirIfnotExists(subDirectory);
            generateTsDeclarations(treeElement.foldercontent, subDirectory)
        }
    })
};
