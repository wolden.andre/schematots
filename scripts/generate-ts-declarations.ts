import * as path from 'path';
import {resolve} from 'path';
import {generateTsDeclarations, getTreeFor, printNewLine} from "./utilityFunctions";
import {PROJECT_DIR, SCHEMA_DIR} from './config'
import {TreeElement} from "./types";

const TS_OUTPUT_DIR = path.resolve(PROJECT_DIR, 'dist', 'ts');

printNewLine();
console.warn("#############################################################");
console.warn("Generating typescript declaration files for all json schemas.");
console.warn("#############################################################");
printNewLine();

console.warn(`Source folder for json schema files: ${SCHEMA_DIR}`);
console.warn(`Output folder: ${TS_OUTPUT_DIR}`);

let tree: TreeElement[] = getTreeFor(SCHEMA_DIR);
console.warn("Schema tree structure:");
printNewLine()
console.warn(JSON.stringify(tree, null, 4));
printNewLine();

generateTsDeclarations(tree, TS_OUTPUT_DIR);
