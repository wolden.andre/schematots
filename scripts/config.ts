export const PROJECT_DIR = `${process.cwd()}`;
export const LIB_DIR = `${PROJECT_DIR}/lib`;
export const LIB_DIR_TYPES = `${LIB_DIR}/typescript`;
export const MOCK_DATA_DIR = `${PROJECT_DIR}/mock_data`;
export const SCHEMA_DIR = `${PROJECT_DIR}/schemas`;
export const DEFINITION_SCHEMA = `${SCHEMA_DIR}/definitions-schema.json`;
export const API_BASE_URL = 'http://localhost:8080';
