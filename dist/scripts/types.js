"use strict";
exports.__esModule = true;
var ElementType;
(function (ElementType) {
    ElementType["FILE"] = "FILE";
    ElementType["FOLDER"] = "FOLDER";
})(ElementType = exports.ElementType || (exports.ElementType = {}));
