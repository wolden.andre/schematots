"use strict";
exports.__esModule = true;
exports.PROJECT_DIR = "" + process.cwd();
exports.LIB_DIR = exports.PROJECT_DIR + "/lib";
exports.LIB_DIR_TYPES = exports.LIB_DIR + "/typescript";
exports.MOCK_DATA_DIR = exports.PROJECT_DIR + "/mock_data";
exports.SCHEMA_DIR = exports.PROJECT_DIR + "/schemas";
exports.DEFINITION_SCHEMA = exports.SCHEMA_DIR + "/definitions-schema.json";
exports.API_BASE_URL = 'http://localhost:8080';
