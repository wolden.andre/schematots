"use strict";
exports.__esModule = true;
var fs = require("fs");
var path_1 = require("path");
var types_1 = require("./types");
var json_schema_to_typescript_1 = require("json-schema-to-typescript");
exports.printNewLine = function () {
    console.warn("\n");
};
exports.isDirectory = function (elementpath) { return fs.existsSync(elementpath) && fs.lstatSync(elementpath).isDirectory(); };
var createDirIfnotExists = function (dir) { return !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true }); };
exports.getTreeFor = function (path) {
    var aDir = fs.readdirSync(path);
    var elementList = aDir.map(function (name) {
        var elementpath = path_1.resolve(path, name);
        return exports.isDirectory(elementpath) ? {
            name: name,
            path: elementpath,
            type: types_1.ElementType.FOLDER,
            foldercontent: exports.getTreeFor(elementpath)
        } : {
            name: name,
            path: elementpath,
            type: types_1.ElementType.FILE
        };
    });
    return elementList;
};
exports.mapKeysAndLocateRefs = function (jsonSchema) {
    var go = function (jsonSchema) {
        var strings = Object.keys(jsonSchema).map(function (a) {
            var jsonSchemaElement = jsonSchema[a];
            if (a === '$ref' && jsonSchemaElement && typeof jsonSchemaElement === 'string') {
                return [jsonSchemaElement];
            }
            else if (jsonSchemaElement && typeof jsonSchemaElement === "object") {
                return go(jsonSchemaElement);
            }
            else {
                return [];
            }
        });
        return strings.flatMap(function (value) { return value; });
    };
    return go(jsonSchema);
};
var addImportStatements = function (treeElement, ts) {
    var buffer = fs.readFileSync(treeElement.path, "utf8");
    var jsonSchema = JSON.parse(buffer);
    var strings = exports.mapKeysAndLocateRefs(jsonSchema);
    strings.forEach(function (typeFilePath) {
        var jsonFilePath = path_1.resolve(treeElement.path, "../", typeFilePath);
        var refBuffer = fs.readFileSync(jsonFilePath, 'utf8');
        var refJson = JSON.parse(refBuffer);
        var NameOfType = refJson['title'];
        if (refJson && NameOfType) {
            ts = "import { " + NameOfType + " } from '" + typeFilePath + "'; \n\n\n" + ts;
        }
    });
    return ts;
};
exports.generateTsDeclarations = function (tree, outputdirectory) {
    createDirIfnotExists(outputdirectory);
    console.warn("Created output directory: " + outputdirectory);
    tree.map(function (treeElement) {
        if (treeElement.type === types_1.ElementType.FILE) {
            json_schema_to_typescript_1.compileFromFile(treeElement.path, { bannerComment: null, declareExternallyReferenced: false })
                .then(function (ts) {
                ts = addImportStatements(treeElement, ts);
                var outputPath = path_1.resolve(outputdirectory, treeElement.name + ".d.ts");
                fs.writeFileSync(outputPath, ts);
                console.warn("Created file: " + outputPath);
            });
        }
        else {
            var subDirectory = path_1.resolve(outputdirectory, treeElement.name);
            createDirIfnotExists(subDirectory);
            exports.generateTsDeclarations(treeElement.foldercontent, subDirectory);
        }
    });
};
